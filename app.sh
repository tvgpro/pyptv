#!/bin/bash

ARGS=""

ARGS="$ARGS --log-to-terminal --log-level info"
ARGS="$ARGS --port 8080"

python bootstrap.py
buildout -U

# exec mod_wsgi-express start-server $ARGS oc_app
exec mod_wsgi-express start-server $ARGS --application-type paste src/sdidemo/production.ini
