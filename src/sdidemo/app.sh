#!/bin/bash

ARGS=""

ARGS="$ARGS --log-to-terminal --log-level info"
ARGS="$ARGS --port 8080"

# exec mod_wsgi-express start-server $ARGS oc_app
exec mod_wsgi-express start-server $ARGS --application-type paste production.ini
